#pragma once
#include "aptacorefactory_global.h"

struct IDiagnosis;
class APTACOREFACTORY_API CoreFactory
{
public:
	CoreFactory();
	virtual ~CoreFactory();

	virtual IDiagnosis* Create(const char* type) = 0;
};


class APTACOREFACTORY_API DiagnosisFactory : public CoreFactory
{
public:
	DiagnosisFactory();
	virtual ~DiagnosisFactory();

	IDiagnosis* Create(const char* type) override;
};
