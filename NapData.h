#pragma once
#include "aptacore_global.h"
#include <vector>
using namespace std;

#  pragma warning( push )
#  pragma warning( disable: 4251 )

class APTACORE_API CNapData
{
public:
	CNapData();
	virtual ~CNapData();

	CNapData(const CNapData& n);
	CNapData& operator=(const CNapData& n);
	void Clear();
	

public:
	string filename;
	vector<double> exprData;
	int classLabel;
	double	Mean;
	double	Median;
	double	stdev;
	double	Min;
	double	Max;
	int		MissvalueCnt;
};

#pragma warning( pop )

