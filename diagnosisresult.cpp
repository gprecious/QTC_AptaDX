#include "diagnosisresult.h"
#include "ui_diagnosisresult.h"
#include <QLibrary>

DiagnosisResult::DiagnosisResult(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DiagnosisResult)
{
    ui->setupUi(this);
}

DiagnosisResult::~DiagnosisResult()
{
    delete ui;
}


//dll 사용 코드


//aptaCoreLung_CAptaCoreLung_t aptaCoreLung_CAptaCoreLung_ptr = NULL;
//aptaCoreLung_setModel_t aptaCoreLung_setModel_ptr = NULL;
//aptaCoreLung_setData_t aptaCoreLung_setData_ptr = NULL;
//aptaCoreLung_run_t aptaCoreLung_run_ptr = NULL;

QLibrary corelib("AptaCoreLung.dll");
typedef void (*aptaCoreLung_CAptaCoreLung_t)();
typedef int (*aptaCoreLung_setModel_t)();
typedef int (*aptaCoreLung_setData_t)();
typedef int (*aptaCoreLung_run_t)();

   aptaCoreLung_CAptaCoreLung_t aptaCoreLung_CAptaCoreLung_ptr = (aptaCoreLung_CAptaCoreLung_t) corelib.resolve("CAptaCoreLung");
   aptaCoreLung_setModel_t aptaCoreLung_setModel_ptr = (aptaCoreLung_setModel_t) corelib.resolve("setModel");
   aptaCoreLung_setData_t aptaCoreLung_setData_ptr = (aptaCoreLung_setData_t) corelib.resolve("setModel");
   aptaCoreLung_run_t aptaCoreLung_run_ptr = (aptaCoreLung_run_t) corelib.resolve("run");


