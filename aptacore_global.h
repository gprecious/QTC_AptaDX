#pragma once

#ifdef APTACORE_EXPORTS
#define APTACORE_API __declspec(dllexport)
#define APTACORE_TEMPLATE
#else
#define APTACORE_API __declspec(dllimport)
#define APTACORE_TEMPLATE extern
#endif
