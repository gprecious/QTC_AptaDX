#pragma once

#ifdef APTACOREANN_EXPORTS
#define APTACOREANN_API __declspec(dllexport)
#else
#define APTACOREANN_API __declspec(dllimport)
#endif