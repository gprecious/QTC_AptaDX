#pragma once

#ifdef APTACOREFACTORY_EXPORTS
#define APTACOREFACTORY_API __declspec(dllexport)
#else
#define APTACOREFACTORY_API __declspec(dllimport)
#endif