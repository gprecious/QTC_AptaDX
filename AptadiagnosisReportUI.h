#pragma once

#include <QWidget>
#include "ui_AptadiagnosisReportUI.h"
#include <QVBoxLayout>

class IDiagnosisResult;
class AptadiagnosisReportUIPrivate;
class AptadiagnosisReportUI : public QWidget
{
	Q_OBJECT

public:
	AptadiagnosisReportUI(QWidget *parent = Q_NULLPTR);
	~AptadiagnosisReportUI();

	QVBoxLayout* verticalLayout();
	void setTitle(QString tilte);
	void setAxisYTitle(QString tilte);
	void setAxisXTitle(QString tilte);
	void setData(const IDiagnosisResult* result);

private:
	Ui::AptadiagnosisReportUI ui;
	AptadiagnosisReportUIPrivate *p;
};
