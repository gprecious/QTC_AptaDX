#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QStringList getSelectedItems(QModelIndexList clickedList);
    QStringList m_modFilesForDetail;
    void checkModsForDetailDiag(QStringList list);

private slots:
    void on_actionOpen_triggered();

    void on_actionSave_triggered();
    void on_FilePushButtonLoadMOD_clicked();
    void on_quitButton_clicked();

    void on_FilePushButtonLoadNAP_clicked();

    void on_FilePushButtonAddMOD_clicked();

    void on_FilePushButtonAddNAP_clicked();

    void on_SinglePushButtonUnloadMOD_clicked();

    void on_SinglePushButtonUnloadNAP_clicked();

    void on_MultiPushButtonUnloadMOD_clicked();

    void on_MultiPushButtonUnloadNAP_clicked();

    void on_DetailPushButtonUnloadNAP_clicked();

    void on_DetailPushButtonUnloadMOD_clicked();

private:
    Ui::MainWindow *ui;


};

#endif // MAINWINDOW_H
