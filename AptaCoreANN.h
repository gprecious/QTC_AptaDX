#pragma once
#include "aptacoreann_global.h"
#include <vector>
#include <string>
using namespace std;

class CAptaCoreAnnPrivate;
class APTACOREANN_API CAptaCoreANN
{
public:
	CAptaCoreANN(void);
	virtual ~CAptaCoreANN(void);
	
	long LoadModel(const char* pszFileName, bool openedWithoutBuild, vector<int>* selectedIdx, vector<string> &aryGroup);
	int ModelTest(double* pdInput, double rdResult[]);
	int GetFeatureNumber();

private:
	CAptaCoreAnnPrivate *p;
};

