#pragma once

#include "aptacore_global.h"
#include <string>
#include <vector>

#  pragma warning( push )
#  pragma warning( disable: 4251 )

struct IValue
{
	std::string label;
	double		value;

	IValue() { label = ""; value = 0; };
	IValue(const char* l, double v) { label = l; value = v; };
	IValue(const IValue &v) { this->label = v.label; this->value = v.value; };
	IValue& operator=(const IValue &v) { this->label = v.label; this->value = v.value; return *this; };
};

class APTACORE_API IDiagnosisValue
{
public:
	IDiagnosisValue();	
	virtual ~IDiagnosisValue();
	IDiagnosisValue(const IDiagnosisValue &v);
	IDiagnosisValue& operator=(const IDiagnosisValue &v);
	IValue& operator[](int index);

	int size() const;
	IValue at(int index) const;
	void push_back(const char* label, double value);	
	virtual const char* label(int index);
	virtual double		value(int index);

private:
	std::vector<IValue> values;
	IValue				temp;
};

class APTACORE_API IDiagnosisResult
{
public:
	IDiagnosisResult();
	virtual ~IDiagnosisResult();

	virtual int size() const;
	virtual IDiagnosisValue at(int index) const;
	virtual void push_back(IDiagnosisValue v);
	virtual void insert(int index, IDiagnosisValue v);
	virtual void rease(int index);

private:
	std::vector<IDiagnosisValue> values;
};


class CModData;
class CNapData;
struct IDiagnosis
{
	virtual int setModel(CModData* data) = 0;
	virtual int setData(CNapData* data) = 0;
	virtual int run() = 0;
	virtual const IDiagnosisResult* getResult() = 0;
};

#pragma warning( pop )