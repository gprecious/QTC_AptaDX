#pragma once
#include "aptacore_global.h"
#include <vector>

#  pragma warning( push )
#  pragma warning( disable: 4251 )

struct AptaItem
{
	std::string subject;
	std::string version;
	std::string format;
	std::string constructor;
	std::string create_date;
	std::string modified_date;
	std::string distribution_date;
	std::string import;

	AptaItem() {}
	AptaItem(AptaItem& data) { operator=(data); };

	AptaItem& operator=(AptaItem& data)
	{
		this->subject           = data.subject;
		this->version			= data.version;
		this->format			= data.format;
		this->constructor		= data.constructor;
		this->create_date		= data.create_date;
		this->modified_date		= data.modified_date;
		this->distribution_date	= data.distribution_date;
		this->import			= data.import;
		return *this;
	};
};

class APTACORE_API CMeta
{
public:
	CMeta();
	CMeta(const CMeta &m);
	CMeta& operator=(const CMeta& m);
	virtual ~CMeta();

public:
	AptaItem header;
	std::vector<AptaItem> files;
	//std::vector<std::string> files;
};


class APTACORE_API CModData
{
public:
	CModData();
	virtual ~CModData();

	int			type;
	CMeta		meta_;
	std::string	version;
	std::string	file;
	std::vector<std::string> files;
	std::vector<bool>		 state;		
};

#pragma warning( pop )
