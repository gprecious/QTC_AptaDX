#include "AptaDiagnosisUI.h"
#include "AptadiagnosisReportUI.h"
#include "AptaCoreLung.h"
#include "AptaCoreFactory.h"
#include "ModData.h"
#include "AptaCoreMOD.h"
#include "AptaCore.h"
#include "NapData.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>

AptaDiagnosisUI::AptaDiagnosisUI(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	
	// connect signal to slot
//	connect(ui.buttonNAP, SIGNAL(clicked()), this, SLOT(buttonNapClicked()));
//	connect(ui.buttonModule, SIGNAL(clicked()), this, SLOT(buttonModuleClicked()));
//	connect(ui.buttonDiagnosis, SIGNAL(clicked()), this, SLOT(buttonDiagnosisClicked()));

	report = new AptadiagnosisReportUI;
	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(report);
	layout->setContentsMargins(QMargins(0, 0, 0, 0));
	ui.frame->setLayout(layout);

	report->setTitle("Lou Gehrig Diagnosis Result");
	report->setAxisYTitle("Lou Gehrig");
	report->setAxisXTitle("(%)");
}

AptaDiagnosisUI::~AptaDiagnosisUI()
{
}

void AptaDiagnosisUI::buttonNapClicked()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open NAP"), "C:/", tr("NAP Files (*.nap)"));
	if (!fileName.isEmpty())
		ui.napPath->setText(fileName);
}

void AptaDiagnosisUI::buttonModuleClicked()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open MDI"), "C:/", tr("MDI Files (*.mdi)"));
	if (!fileName.isEmpty())
		ui.mdiPath->setText(fileName);
}

void AptaDiagnosisUI::buttonDiagnosisClicked()
{
	string mdiPath = ui.mdiPath->text().toLocal8Bit().toStdString();
	string napPath = ui.napPath->text().toLocal8Bit().toStdString();

	CAptaCore core; 

	CAptaCoreMOD mod;
	CModData *model = mod.LoadMOD(mdiPath.c_str());
	if (!model)
	{
		QMessageBox::warning(this, tr("AptaDX Diagnosis"), tr("The mdi file does not exist."), QMessageBox::StandardButton::Ok);
		return;
	}

	CNapData napData;
	int ret = core.LoadNap(napPath.c_str(), &napData);
	if (!ret)
	{
		QMessageBox::warning(this, tr("AptaDX Diagnosis"), tr("The nap file does not exist."), QMessageBox::StandardButton::Ok);
		return;
	}

	CoreFactory* fc = new DiagnosisFactory;
	IDiagnosis *diagnosis = fc->Create("Lung");
	if (!diagnosis)
	{
		QMessageBox::warning(this, tr("AptaDX Diagnosis"), tr("The diagnostic function can not be called"), QMessageBox::StandardButton::Ok);
		return;
	}

	CNapData napLog;
	napLog = core.LogTranceform(&napData);

	CNapData napNor;
	napNor = core.Normalization(&napLog);

		
	diagnosis->setModel(model);
	diagnosis->setData(&napNor);
	diagnosis->run();

	const IDiagnosisResult* result = diagnosis->getResult();
	report->setData(result);

#if _DEBUG
	if (result)
	{
		for (int i = 0; i < (int)result->size(); i++)
		{
			IDiagnosisValue value = result->at(i);
			//printf("result : %s[%0.2f], %s[%0.2f]\n", value[0].label.c_str(), value[0].value, value[1].label.c_str(), value[1].value);
			qDebug() << "result :" << value[0].label.c_str() << "[" << value[0].value << "], " << value[1].label.c_str() << "[" << value[1].value << "]";
		}
	}
#endif
}
