#include "AptadiagnosisReportUI.h"
#include <QChartView>
#include <QBarSeries>
#include <QHorizontalBarSeries>
#include <QBarSet>
#include <QLegend>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QDebug>
#include "IDiagnosis.h"
using namespace QtCharts;


class AptadiagnosisReportUIPrivate
{
public:
	AptadiagnosisReportUIPrivate() {};
	~AptadiagnosisReportUIPrivate() {};
	void clear();

	QChart *chart;
	QChartView *chartView;
	QStringList categories;
	QBarCategoryAxis *axisY;
	QValueAxis *axisX;
	//QHorizontalBarSeries *series;
	QBarSeries *series;
	QBarSet *set;
};

void AptadiagnosisReportUIPrivate::clear()
{
    series->remove(set);
    axisY->clear();
    categories.clear();
}


AptadiagnosisReportUI::AptadiagnosisReportUI(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	
	p = new AptadiagnosisReportUIPrivate;	

	//p->series = new QHorizontalBarSeries;
	p->series = new QBarSeries;
	p->axisY = new QBarCategoryAxis;
	p->axisX = new QValueAxis;

	p->chart = new QChart;
	p->chart->addSeries(p->series);
	//p->chart->setAxisY(p->axisY, p->series);
	//p->chart->setAxisX(p->axisX, p->series);
	p->chart->setAxisY(p->axisX, p->series);
	p->chart->setAxisX(p->axisY, p->series);
	p->chart->setAnimationOptions(QChart::NoAnimation);
	p->chart->legend()->setVisible(false);
	p->chart->legend()->setAlignment(Qt::AlignBottom);	

	p->chartView = new QChartView(p->chart);
	p->chartView->setRenderHint(QPainter::Antialiasing);
	
	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(p->chartView);
	ui.frame->setLayout(layout);
}

AptadiagnosisReportUI::~AptadiagnosisReportUI()
{
}

QVBoxLayout* AptadiagnosisReportUI::verticalLayout()
{
	return ui.verticalLayout;
}

void AptadiagnosisReportUI::setTitle(QString tilte)
{
	p->chart->setTitle(tilte);
}

void AptadiagnosisReportUI::setData(const IDiagnosisResult* result)
{
	qDebug() << "--------------------------------";
	qDebug() << "AptadiagnosisReportUI::setData()";

	if (result)
	{
		p->clear();

		p->set = new QBarSet("apta");
		for (int i = 0; i < (int)result->size(); i++)
		{
			IDiagnosisValue value = result->at(i);
			*p->set << value[0].value << value[1].value;
			p->categories << value[0].label.c_str() << value[1].label.c_str();

			qDebug() << "setData :" << value[0].label.c_str() << "[" << value[0].value << "], " << value[1].label.c_str() << "[" << value[1].value << "]";
		}

		// set dummy data
		int index = 1;
		for (int i = 0; i < 4 - (int)result->size(); i++)
		{
			*p->set << 0.0 << 0.0;
			p->categories << QString("dummy %1").arg(index++) << QString("dummy %1").arg(index++);
		}
			
		p->series->append(p->set);
		p->axisY->append(p->categories);		
	}
}

void AptadiagnosisReportUI::setAxisYTitle(QString tilte)
{
	p->axisY->setTitleText(tilte);	
}

void AptadiagnosisReportUI::setAxisXTitle(QString tilte)
{
	p->axisX->setTitleText(tilte);
	p->axisX->applyNiceNumbers();
	p->axisX->setRange(0.0, 1.0);
}
