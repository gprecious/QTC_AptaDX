#include "aptacorelung_global.h"
#include "IDiagnosis.h"

class CAptaCoreLungPrivate;
class APTACORELUNG_API CAptaCoreLung : public IDiagnosis
{
public:
	CAptaCoreLung(void);
	virtual ~CAptaCoreLung(void);

public: // override
	int setModel(CModData* data) override;
	int setData(CNapData* data) override;
	int run() override;
	const IDiagnosisResult* getResult() override;
	
private:
	CAptaCoreLungPrivate *p;
};

