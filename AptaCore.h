#pragma once
#include "aptacore_global.h"
#include <vector>
using namespace std;

class CNapData;
class APTACORE_API CAptaCore
{
public:
	CAptaCore(void);

	int LoadNap(const char *file, CNapData* data);
	int LoadNap(const wchar_t *file, CNapData* data);
	int LoadNap(const wchar_t *file, vector<CNapData> *data) { return 0; }
	CNapData LogTranceform(CNapData* data);
	CNapData Normalization(CNapData* data);
	int OneWayANOVA(CNapData* data);
};

#define SAFE_DELETE(p) {if((p)){ delete (p); (p)=0;} else { (p) = 0;}}

namespace APTA
{
	APTACORE_API const char* GetPathA(const char* str);
	APTACORE_API int Exec(const char* cmd);
}