#pragma once
#include "aptacoremod_global.h"

#define UNCOMPRESS_PATH "$(ROOT)/uncompress"
#define ZIP_PATH		"$(ROOT)/7-zip"

class CModData;
class APTACOREMOD_API CAptaCoreMOD {
public:
	CAptaCoreMOD(void);
	
	CModData* LoadMOD(const char * file);
	CModData* LoadMOD(const wchar_t * file);
};
