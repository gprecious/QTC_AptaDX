#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QStringListModel>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui; 
}
QStringList MainWindow::getSelectedItems(QModelIndexList clickedIndex) {
    QStringList clickedList;

    foreach (const QModelIndex &index, clickedIndex) {
        clickedList.append(index.data(Qt::DisplayRole).toString());
    }
    return clickedList;
}
void MainWindow::checkModsForDetailDiag(QStringList list) {
    for(int i=0; i<list.size(); i++) {
        if(i == 0) {
            if(list.at(i).contains("1.mod", Qt::CaseInsensitive) == true) {
                ui->DetailCheckBox1->setChecked(1);
            } else {
                ui->DetailCheckBox1->setChecked(0);
            }
        } else if(i == 1) {
            if(list.at(i).contains("2.mod", Qt::CaseInsensitive) == true) {
                ui->DetailCheckBox2->setChecked(1);
            } else {
                ui->DetailCheckBox2->setChecked(0);
            }
        } else if(i == 2) {
            if(list.at(i).contains("3.mod", Qt::CaseInsensitive) == true) {
                ui->DetailCheckBox3->setChecked(1);
            } else {
                ui->DetailCheckBox3->setChecked(0);
            }
        } else if(i == 3) {
            if(list.at(i).contains("4.mod", Qt::CaseInsensitive) == true) {
                ui->DetailCheckBox4->setChecked(1);
            } else {
                ui->DetailCheckBox4->setChecked(0);
            }
        } else if(i == 4) {
            if(list.at(i).contains("5.mod", Qt::CaseInsensitive) == true) {
                ui->DetailCheckBox5->setChecked(1);
            } else {
                ui->DetailCheckBox5->setChecked(0);
            }
        } else if(i == 5) {
            if(list.at(i).contains("6.mod", Qt::CaseInsensitive) == true) {
                ui->DetailCheckBox6->setChecked(1);
            } else {
                ui->DetailCheckBox6->setChecked(0);
            }
        } else if(i == 6) {
            if(list.at(i).contains("7.mod", Qt::CaseInsensitive) == true) {
                ui->DetailCheckBox7->setChecked(1);
            } else {
                ui->DetailCheckBox7->setChecked(0);
            }
        } else if(i == 7) {
            if(list.at(i).contains("8.mod", Qt::CaseInsensitive) == true) {
                ui->DetailCheckBox8->setChecked(1);
            } else {
                ui->DetailCheckBox8->setChecked(0);
            }
        }


    }
}

void MainWindow::on_actionOpen_triggered()
{
//    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString(),
//                tr("Text Files (*.txt);;C++ Files (*.cpp *.h)")); 

//    if (!fileName.isEmpty()) {
//        QFile file(fileName);
//        if (!file.open(QIODevice::ReadOnly)) {
//            QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
//            return;
//        } 
//        QTextStream in(&file);
//        ui->textEdit->setText(in.readAll());
//        file.close();
//    }
}

void MainWindow::on_actionSave_triggered()
{
//    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), QString(),
//                tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));

//        if (!fileName.isEmpty()) {
//            QFile file(fileName);
//            if (!file.open(QIODevice::WriteOnly)) {
//                // error message
//            } else {
//                QTextStream stream(&file);
//                stream << ui->textEdit->toPlainText();
//                stream.flush();
//                file.close();
//            }
//        }
}

void MainWindow::on_quitButton_clicked()
{

}

void MainWindow::on_FilePushButtonLoadMOD_clicked()
{
    //fileName = QFileDialog::getOpenFileName(this,
      //  tr("Load MOD files"), "/home/jana", tr("Image Files (*.mod)"));
   //QFileDialog dlg(this);
   //dlg.setNameFilter(tr("Model (*.mod *.mdi)"));

    QStringList selectedMods =  QFileDialog::getOpenFileNames(this,tr("MOD files"),QDir::currentPath(),tr("MOD files (*.mod *.mdi)") );
    QStringListModel *listModel = new QStringListModel(selectedMods, NULL);
    ui->FileListViewMOD->setModel(listModel);

    //selectedMods->append("xyz");
    //listModel->setStringList(*selectedMods);


}

void MainWindow::on_FilePushButtonLoadNAP_clicked()
{
    QStringList selectedNaps =  QFileDialog::getOpenFileNames(this,tr("NAP files"),QDir::currentPath(),tr("NAP files (*.nap)") );
    QStringListModel *listModel = new QStringListModel(selectedNaps, NULL);
    ui->FileListViewNAP->setModel(listModel);
}

void MainWindow::on_FilePushButtonAddMOD_clicked()
{
    int curIndex = ui->tabWidget->currentIndex();
    int selectedSize = ui->FileListViewMOD->selectionModel()->selectedIndexes().size();
    QModelIndexList clickedIndex = ui->FileListViewMOD->selectionModel()->selectedIndexes();
    QStringList clickedMods = getSelectedItems(clickedIndex);

    if(curIndex == 0) {
        if(selectedSize > 1) {
            QMessageBox msg;
            msg.setText("단일 진단 시 MOD 파일은 1개만 선택하여 주십시오.");
            msg.exec();
        } else {

            //qDebug() << clickedMods.join(",");
            ui->SingleLineEditMOD->setText(clickedMods.at(0));
        }
    } else if(curIndex == 1) {
        if(selectedSize > 1) {
            QMessageBox msg;
            msg.setText("일괄 진단 시 MOD 파일은 1개만 선택하여 주십시오.");
            msg.exec();
        } else {

            //qDebug() << clickedMods.join(",");
            ui->MultiLineEditMOD->setText(clickedMods.at(0));
        }
    } else if(curIndex == 2 ) {
        if(selectedSize != 8) {
            QMessageBox msg;
            msg.setText("세부 진단 시 MOD 파일은 8개를 선택해야 합니다.");
            msg.exec();
        } else {
            m_modFilesForDetail = clickedMods;

            QStringListModel *listModel = new QStringListModel(clickedMods, NULL);
            ui->DetailListViewMOD->setModel(listModel);

            checkModsForDetailDiag(m_modFilesForDetail);
        }
    }
}

void MainWindow::on_FilePushButtonAddNAP_clicked()
{
    int curIndex = ui->tabWidget->currentIndex();
    int selectedSize = ui->FileListViewNAP->selectionModel()->selectedIndexes().size();
    QModelIndexList clickedIndex = ui->FileListViewNAP->selectionModel()->selectedIndexes();
    QStringList clickedNaps = getSelectedItems(clickedIndex);

    if(curIndex == 0) {
        if(selectedSize > 1) {
            QMessageBox msg;
            msg.setText("단일 진단 시 NAP 파일은 1개만 선택하여 주십시오.");
            msg.exec();
        } else {

            //qDebug() << clickedMods.join(",");
            ui->SingleLineEditNAP->setText(clickedNaps.at(0));
        }
    } else if(curIndex == 1) {
        if(selectedSize < 2) {
            QMessageBox msg;
            msg.setText("일괄 진단 시 NAP 파일은 2개 이상을 선택하여 주십시오.");
            msg.exec();
        } else {

            QStringListModel *listModel = new QStringListModel(clickedNaps, NULL);
            ui->MultiListViewNAP->setModel(listModel);

        }
    } else if(curIndex == 2 ) {
        if(selectedSize > 1) {
            QMessageBox msg;
            msg.setText("세부 진단 시 NAP 파일은 1개만 선택하여 주십시오.");
            msg.exec();
        } else {
            ui->DetailLineEditNAP->setText(clickedNaps.at(0));
        }
    }
}

void MainWindow::on_SinglePushButtonUnloadMOD_clicked()
{
    ui->SingleLineEditMOD->setText(tr(""));
}

void MainWindow::on_SinglePushButtonUnloadNAP_clicked()
{
    ui->SingleLineEditNAP->setText(tr(""));
}

void MainWindow::on_MultiPushButtonUnloadMOD_clicked()
{
    ui->MultiLineEditMOD->setText(tr(""));
}

void MainWindow::on_MultiPushButtonUnloadNAP_clicked()
{
    //qDebug() << ui->MultiListViewNAP->currentIndex().row();
    QModelIndexList selected = ui->MultiListViewNAP->selectionModel()->selectedIndexes();

    while(selected.size()) {
        ui->MultiListViewNAP->model()->removeRow(selected.first().row());
        selected = ui->MultiListViewNAP->selectionModel()->selectedIndexes();
    }
}

void MainWindow::on_DetailPushButtonUnloadNAP_clicked()
{
    ui->DetailLineEditNAP->setText(tr(""));
}

void MainWindow::on_DetailPushButtonUnloadMOD_clicked()
{
    QModelIndexList selected = ui->DetailListViewMOD->selectionModel()->selectedIndexes();

    while(selected.size()) {
        ui->DetailListViewMOD->model()->removeRow(selected.first().row());
        selected = ui->DetailListViewMOD->selectionModel()->selectedIndexes();
    }

    //checkModsForDetailDiag
}
