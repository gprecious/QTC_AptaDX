#pragma once

#include <QDialog>
#include "ui_AptaDiagnosisUI.h"

class AptadiagnosisReportUI;
class AptaDiagnosisUI : public QDialog
{
	Q_OBJECT

public:
	AptaDiagnosisUI(QWidget *parent = Q_NULLPTR);
	~AptaDiagnosisUI();

public slots:
	void buttonNapClicked();
	void buttonModuleClicked();
	void buttonDiagnosisClicked();

private:
	Ui::AptaDiagnosisUI ui;
	AptadiagnosisReportUI *report;
};
