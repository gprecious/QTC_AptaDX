#-------------------------------------------------
#
# Project created by QtCreator 2017-08-08T17:18:40
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QT_AptaDX
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    diagnosisresult.cpp \
    AptadiagnosisReportUI.cpp \
    AptaDiagnosisUI.cpp

HEADERS += \
        mainwindow.h \
    diagnosisresult.h \
    AptadiagnosisReportUI.h \
    AptaDiagnosisUI.h \
    IDiagnosis.h \
    aptacore_global.h \
    AptaCore.h \
    AptaCoreFactory.h \
    AptaCoreLung.h \
    AptaCoreMOD.h \
    ModData.h \
    NapData.h \
    aptacorelung_global.h \
    aptacoreann_global.h \
    aptacoremod_global.h \
    aptacorefactory_global.h \
    AptaCoreANN.h

FORMS += \
        mainwindow.ui \
    diagnosisresult.ui \
    AptadiagnosisReportUI.ui \
    AptaDiagnosisUI.ui

INCLUDEPATH += D:/qt/5.8/msvc2015/include/QtCharts

unix|win32: LIBS += -L$$PWD/./ -lAptaCore

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

unix|win32: LIBS += -L$$PWD/./ -lAptaCoreANN

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

unix|win32: LIBS += -L$$PWD/./ -lAptaCoreFactory

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

unix|win32: LIBS += -L$$PWD/./ -lAptaCoreLung

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

unix|win32: LIBS += -L$$PWD/./ -lAptaCoreMOD

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.
