#ifndef DIAGNOSISRESULT_H
#define DIAGNOSISRESULT_H

#include <QDialog>

namespace Ui {
class DiagnosisResult;
}

class DiagnosisResult : public QDialog
{
    Q_OBJECT

public:
    explicit DiagnosisResult(QWidget *parent = 0);
    ~DiagnosisResult();

private:
    Ui::DiagnosisResult *ui;
};

#endif // DIAGNOSISRESULT_H
