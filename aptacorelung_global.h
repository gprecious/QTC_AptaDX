#pragma once

#ifdef APTACORELUNG_EXPORTS
#define APTACORELUNG_API __declspec(dllexport)
#else
#define APTACORELUNG_API __declspec(dllimport)
#endif